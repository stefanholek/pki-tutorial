References
==========

* `openssl req configuration file options <https://www.openssl.org/docs/manmaster/man1/openssl-req.html#CONFIGURATION-FILE-FORMAT>`_
* `X.509 v3 extension configuration format <https://www.openssl.org/docs/manmaster/man5/x509v3_config.html>`_


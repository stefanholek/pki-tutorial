===============================
Appendix C: X.509 v3 Extensions
===============================

The X.509 v3 format defines a set of extensions for certificates,
certificate signing requests (CSR), and certificate revocation lists (CRL).
[:rfc:`5280#section-4.2`]

Such extensions:

a. Define type and purpose of a certificate, CSR, or CRL.
b. Define restrictions on the applicability of a certificate or CRL.
c. Provide pointers to issuer and revocation information for a
   certificate or CRL.

The most commonly encountered extensions are discussed below:

.. _keyUsage:

keyUsage
=================

MUST be present in all certificates and CSRs.
Always critical.

CA certificates use ``keyCertSign`` and ``cRLSign``. User certificates use
``digitalSignature`` and ``keyEncipherment``.

.. _basicConstraints:

basicConstraints
=========================

MUST be present in all certificates.
MAY be present in CSRs.
Critical in CA certificates.

The extension has two values:

* ``CA`` which is a boolean value set to TRUE for CA certificates and FALSE for
  user certificates. Always present [#]_.
* ``pathlen`` which is an integer value defining the number of CAs allowed
  below the CA carrying the extension. MAY be present in non-root CA
  certificates.

.. rubric:: Footnotes

.. [#] The RFC says not to include the extension in user certificates
       but is commonly ignored.

.. _extendedKeyUsage:

extendedKeyUsage (EKU)
=========================

MAY be present in non-root certificates and CSRs [#]_.
Critical or not depending on purpose.

Together with :ref:`keyUsage` and :ref:`basicConstraints` this extension
controls how a certificate may be used.
Defined purposes are: ``emailProtection``, ``serverAuth``, ``clientAuth``,
``codeSigning``, ``timeStamping``, and ``OCSPSigning``.
The latter three MUST be marked critical in user certificates.

.. rubric:: Footnotes

.. [#] While typically only present in user certificates, some PKIs have
       started to use the extension in CA certificates as well (never
       critical).

.. _subjectKeyIdentifier:

subjectKeyIdentifier
=============================

MUST be present in all certificates and CSRs.
Never critical.

Key ID derived from the hash of the subject's public key.

.. _authorityKeyIdentifier:

authorityKeyIdentifier
===============================

MUST be present in all certificates and CRLs.
Never critical.

Key ID derived from the hash of the issuer's public key.

.. _subjectInfoAccess:

subjectInfoAccess (SIA)
==========================

MAY be present in all certificates and CSRs.
Never critical.

The extension has two values:

* ``caRepository`` points to cross-certificates issued by the CA in the certificate subject.
* ``timeStamping`` points to a time-stamping service offered by the certificate subject.

.. _authorityInfoAccess:

authorityInfoAccess (AIA)
============================

MAY be present in non-root certificates and CRLs.
Never critical.

The extension has two values:

* ``caIssuers`` points to certificates issued to the CA that has issued the certificate or CRL.
* ``OCSP`` points to an OCSP responder covering the CA that has issued the certificate.

.. _crlDistributionPoints:

crlDistributionPoints
==============================

MAY be present in non-root certificates.
Never critical.

Points to the CRL issued by the CA that has issued the certificate.

.. _subjectAltName:

subjectAltName (SAN)
=======================

MAY be present in all certificates and CSRs.
Critical if subject DN is empty.

Contains names associated with the certificate's subject, that can or should
not be part of the DN. This includes Internet domain names, email addresses,
and URIs.

.. _issuerAltName:

issuerAltName (IAN)
=======================

MAY be present in all certificates and CRLs.
Never critical.

Contains names associated with the certificate's issuer, that can or should
not be part of the DN. This includes Internet domain names, email addresses,
and URIs.

.. _nameConstraints:

nameConstraints
========================

MAY be present in CA certificates and cross-certificates.
SHOULD be marked critical.

Defines a namespace within which all subsequent subject names in the
certificate path must reside.

Self-signed root certificates are not considered in the name validation
process unless the certificate is the final certificate in the path.

.. _certificatePolicies:

certificatePolicies
============================

MAY be present in non-root certificates.
SHOULD be marked critical.

Certificate policies are labels attached to the certificate
path. To be valid, a policy must be present in every certificate
along the path to the root CA. A policy
has no meaning outside of what the PKI-owner wants it to mean.
The extension SHOULD be marked critical, but usually
isn't out of compatibility concerns.

Self-signed root certificates are not considered in the policy
validation process and never have a certificatePolicies extension.

.. _policyMappings:

policyMappings
=======================

MAY be present in cross-certificates.
SHOULD be marked critical.

Provides a way to map certificate policies between PKIs.
The extension SHOULD be marked critical, but usually
isn't out of compatibility concerns.


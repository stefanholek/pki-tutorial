=======================
Appendix B: CA Database
=======================

Index File
----------

The index file consists of zero or more lines,
each containing the following fields separated by tab characters:

#. Certificate status flag (V=valid, R=revoked, E=expired).
#. Certificate expiration date in [YY]YYMMDDHHMMSSZ format.
#. Certificate revocation date in [YY]YYMMDDHHMMSSZ[,reason] format. Empty if not
   revoked.
#. Certificate serial number in hex.
#. Certificate filename or literal string 'unknown'.
#. Certificate subject DN.

The ``openssl ca`` command uses this file as certificate database.

The filename field appears to be unused and always
contains the string 'unknown'. Certificates are stored in the certificate
archive (``new_certs_dir``) under the name <serial number>.pem.

Attribute File
--------------

The attribute file contains a single line: ``unique_subject = no``. It
reflects the setting in the CA section of the configuration file at the time
the first record is added to the database.

Serial Number Files
-------------------

The ``openssl ca`` command uses two serial number files:

#. Certificate serial number file.
#. CRL number file.

The files contain the next available serial number in hex.

If the CA section sets ``rand_serial = yes`` the contents of the
certificate serial number file are ignored; it must still exist though.

Limitations
-----------

#. The entire database must fit into memory.
#. There are no provisions for concurrency handling.

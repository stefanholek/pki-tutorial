What's New in v2.0?
=======================

Tutorial
--------

- Add :doc:`extensions`.

Examples
--------

- All: Upgrade default MD to SHA256.
- All: Allow emailAddress in user cert subject DNs. [#]_
- All: Add ``rand_serial = yes`` to CA sections. Requires OpenSSL 1.1.
- All: Use example.com domain in default values.

- Simple: Remove organizationalUnitName from CA DNs.
- Simple: Display UTF-8 characters in DNs.

- Advanced: Remove anyExtendedKeyUsage from email certs.
- Advanced: Add userId to code-signing cert DN.
- Advanced: Reduce TLS cert lifetime to 1 year.
- Advanced: Change CA base_url.
- Advanced: Inline subject DN display options.

- Expert: Add dataEnciperment key-usage to encryption certs.
- Expert: Add nonRepudiation key-usage to time-stamping certs.
- Expert: Add OCSP info to identity and encryption certs.
- Expert: Add OCSP info and SIA to time-stamping certs.
- Expert: Add high-assurance and time-stamping policies.
- Expert: Reduce identity cert lifetime to 2 years.
- Expert: Reduce component cert lifetime to 1 year.
- Expert: Inline subject DN display options.

.. rubric:: Footnotes

.. [#] Some address books and email clients do not deal well with subjectAltName.
       We have repeatedly been unable to find certificates by email address
       unless it was present in the DN. To restore the old behavior
       set ``subjectAltName = email:move`` in CSR configs and
       ``email_in_dn = no`` in signing CA configs.

Scripts
-------

- Please see the `PKI-Scripts
  <https://bitbucket.org/stefanholek/pki-scripts>`_ repository for more.

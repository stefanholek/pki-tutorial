==========================
Root CA Configuration File
==========================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-1/etc/root-ca.conf

.. include:: ../refs-ca.rst


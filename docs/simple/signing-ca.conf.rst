=============================
Signing CA Configuration File
=============================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-1/etc/signing-ca.conf

.. include:: ../refs-ca.rst


============================================
Email Certificate Request Configuration File
============================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-1/etc/email.conf

.. include:: ../refs-req.rst


=================================================
TLS Server Certificate Request Configuration File
=================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-1/etc/server.conf

.. include:: ../refs-req.rst


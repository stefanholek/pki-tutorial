==========================
Root CA Configuration File
==========================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/root-ca.conf

.. include:: ../refs-ca.rst


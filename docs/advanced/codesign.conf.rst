===================================================
Code-Signing Certificate Request Configuration File
===================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/codesign.conf

.. include:: ../refs-req.rst


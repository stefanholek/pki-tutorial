===========================
Email CA Configuration File
===========================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/email-ca.conf

.. include:: ../refs-ca.rst


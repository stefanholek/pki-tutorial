==============================
Software CA Configuration File
==============================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/software-ca.conf

.. include:: ../refs-ca.rst


=================================================
TLS Server Certificate Request Configuration File
=================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/server.conf

.. include:: ../refs-req.rst


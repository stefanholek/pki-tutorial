=================================================
TLS Client Certificate Request Configuration File
=================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/client.conf

.. include:: ../refs-req.rst


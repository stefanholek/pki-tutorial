============================================
Email Certificate Request Configuration File
============================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/email.conf

.. include:: ../refs-req.rst


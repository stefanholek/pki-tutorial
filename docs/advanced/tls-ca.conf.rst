=============================
TLS CA Configuration File
=============================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-2/etc/tls-ca.conf

.. include:: ../refs-ca.rst


====================================================
Time-Stamping Certificate Request Configuration File
====================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/timestamp.conf

.. include:: ../refs-req.rst


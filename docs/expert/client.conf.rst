=================================================
TLS Client Certificate Request Configuration File
=================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/client.conf

.. include:: ../refs-req.rst


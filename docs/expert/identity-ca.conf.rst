===================================
Identity CA Configuration File
===================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/identity-ca.conf

.. include:: ../refs-ca.rst


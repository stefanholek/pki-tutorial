==================================
Network CA Configuration File
==================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/network-ca.conf

.. include:: ../refs-ca.rst


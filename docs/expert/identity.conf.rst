===============================================
Identity Certificate Request Configuration File
===============================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/identity.conf

.. include:: ../refs-req.rst


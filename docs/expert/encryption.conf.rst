=================================================
Encryption Certificate Request Configuration File
=================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/encryption.conf

.. include:: ../refs-req.rst


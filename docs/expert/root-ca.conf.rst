===============================
Root CA Configuration File
===============================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/root-ca.conf

.. include:: ../refs-ca.rst


===================================================
OCSP-Signing Certificate Request Configuration File
===================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/ocspsign.conf

.. include:: ../refs-req.rst


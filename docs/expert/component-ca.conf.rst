====================================
Component CA Configuration File
====================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/component-ca.conf

.. include:: ../refs-ca.rst


=================================================
TLS Server Certificate Request Configuration File
=================================================

.. highlight:: openssl

.. literalinclude:: ../../pki-example-3/etc/server.conf

.. include:: ../refs-req.rst


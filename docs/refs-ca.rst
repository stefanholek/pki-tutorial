References
==========

* `openssl req configuration file options <https://www.openssl.org/docs/manmaster/man1/openssl-req.html#CONFIGURATION-FILE-FORMAT>`_
* `openssl ca configuration file options <https://www.openssl.org/docs/manmaster/man1/openssl-ca.html#CONFIGURATION-FILE-OPTIONS>`_
* `X.509 v3 extension configuration format <https://www.openssl.org/docs/manmaster/man5/x509v3_config.html>`_


from setuptools import setup

version = '2.0'

setup(name='pki-tutorial',
      version=version,
      description='OpenSSL PKI Tutorial',
      author='Stefan H. Holek',
      author_email='stefan@epy.co.at',
      url='https://pki-tutorial.readthedocs.io/',
      license='CC',
      zip_safe=False,
      install_requires=[
          'sphinx==5.3.0',
          'pygments-openssl>=1.5',
      ],
)
